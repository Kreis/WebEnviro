from xml.dom import minidom
import json, sys

if len(sys.argv) < 3:
	print "Bad argument"
	print "[ 1 ] read xml file"
	print "[ 2 ] name output json file"
	exit

XML_FILE_NAME = sys.argv[ 1 ]
JSON_FILE_NAME = sys.argv[ 2 ]

def get_element(father, name_son):
	return father.getElementsByTagName(name_son)
def get_attribute(node, name_attribute):
	return node.attributes[name_attribute].value
def get_attributes(node, names_attributes, out):
	for x in names_attributes:
		out[ x ] = get_attribute(node, x)

xmldoc = minidom.parse(XML_FILE_NAME)
font = get_element(xmldoc, 'font')[ 0 ]

common = get_element(font, 'common')[ 0 ]

final_json = {}
final_json['font'] = {}
final_json['font']['common'] = {}
list_attributes = [
'scaleW',
'scaleH',
'lineHeight',
'pages']
get_attributes(common, list_attributes, final_json['font']['common'])

final_json['font']['pages'] = []
pages = get_element(font, 'pages')[ 0 ]
pages = get_element(font, 'page')
id_pages = 0
for page in pages:
	final_json['font']['pages'].append({})
	get_attributes(page, ['id', 'file'], final_json['font']['pages'][ id_pages ])
	id_pages += 1

final_json['font']['chars'] = {}
chars = get_element(font, 'chars')[ 0 ]
final_json['font']['chars_count'] = get_attribute(chars, 'count')
chars = get_element(chars, 'char')
list_attributes = ['xadvance', 'x', 'y', 'yoffset', 'xoffset', 'page', 'height', 'width']
for char in chars:
	ascii_id = get_attribute(char, 'id')
	final_json['font']['chars'][ ascii_id ] = {}
	get_attributes(char, list_attributes, final_json['font']['chars'][ ascii_id ])

final_json['font']['kernings'] = {}
kernings = get_element(font, 'kernings')[ 0 ]
final_json['font']['kernings_count'] = get_attribute(kernings, 'count')
kernings = get_element(kernings, 'kerning')
for kerning in kernings:
	first_letter = get_attribute(kerning, 'first');
	second_letter = get_attribute(kerning, 'second');
	complex_id = str(first_letter)+'-'+str(second_letter);
	final_json['font']['kernings'][ complex_id ] = get_attribute(kerning, 'amount')

file_object = open(JSON_FILE_NAME, 'w')
file_object.write(json.dumps(final_json))
file_object.close()
