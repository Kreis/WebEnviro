window.enviro = window.enviro || {};
window.enviro.local_state = window.enviro.local_state || {};

var StateHandler = function() {
	this.local_state = {
		'x': 100,
		'y': 400,
		'dir_x': 0,
		's_jmp': 0,
		'flip_x': false,
		'sit': false
	};
	this.private_last_sended_state = {};
	this.private_list_field = [
	'x', 'y', 'dir_x', 's_jmp', 'flip_x', 'sit'
	];
};

StateHandler.prototype.get = function(name) {
	return this.local_state[ name ];
};

StateHandler.prototype.put = function(name, value) {
	this.local_state[ name ] = value;
};

StateHandler.prototype.get_local_state = function() {
	return this.local_state;
};

enviro.state_handler = new StateHandler();