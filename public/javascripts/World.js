
var World = function() {
	this.entities = [];
	this.other_active_players = {};
	this.physics = new Physics();
};

World.prototype.start = function() {

	// Fonthandler
	var font_handler = new FontHandler();
	font_handler.initialize({'x': 200, 'y': 200, 'width': 400, 'height':100}, 'default.json');
	font_handler.set_text('hola esto es un simulacro', 0.2);
	this.entities.push(font_handler);


	// PJS
	var my_pj = new Avatar(this.physics);
	my_pj.start(new enviro.InputListener(), 500, 500);
	this.entities.push(my_pj);

	var go = { 
		entities: this.entities,
		physics: this.physics,
		other_active_players: this.other_active_players
	 };
	var other_player_login = function(state) {

		if ( ! enviro.player_id || enviro.player_id == -1)
			return;

		for (var key in go.other_active_players)
			go.other_active_players[ key ].active = false;

		for (var key in state) {

			if (key == enviro.player_id)
				continue;

			if (go.other_active_players[ key ]) {
				go.other_active_players[ key ].active = true;
				continue;
			}

			var other_pj = new Avatar(go.physics);
			other_pj.id = key;

			other_pj.start(new enviro.InputSocketListener(state[ key ]), state[ key ].x, state[ key ].y);
			go.entities.push(other_pj);
			
			go.other_active_players[ key ] = { active: true };
		}

		for (var key in go.other_active_players) if ( ! go.other_active_players[ key ].active) {
			
			var killing_entity = go.entities.find(function(entity) {
				return entity.id && entity.id == key;
			});

			killing_entity.destructor();

			var id_eliminated_entity = go.entities.indexOf(killing_entity);
			if (id_eliminated_entity >= 0)
				go.entities.splice(id_eliminated_entity, 1);

			delete go.other_active_players[ key ];
		}
	};

	enviro.socket_handler.add_listener('on_state', other_player_login);

	var box = new Box(0, 0, 1024, 600);

	var platform = new Platform(this.physics, 100, 560, 100, 10);
	this.entities.push(platform);

	this.physics.add_rigid_body(box);

	var evv = function(x, y) {
		console.log('here: ' + x + ' ' + y);
		font_handler.next();
	};
	enviro.mouse_handler.add_listener(TypeEvent.MOUSE_TYPED, evv);
};

World.prototype.update = function(delta) {
	this.physics.update();

	for (var i = 0; i < this.entities.length; i++) {

		if ( ! this.entities[ i ])
			continue;

		this.entities[ i ].update(delta * 0.0166666);
	}

	enviro.socket_handler.emit_state();
};