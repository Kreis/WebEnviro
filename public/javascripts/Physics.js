var ParabolaType = {
	OPEN_UP: 1,
	OPEN_DOWN: 2,
	OPEN_LEFT: 3,
	OPEN_RIGHT: 4,
};

var Parabola = function(rigid_body, max_height, time_to_peak, parabola_type, elapsed_time = 0, time_to_stop = -1) {
	this.rigid_body = rigid_body;
	this.g = (max_height * -2) / (time_to_peak * time_to_peak);
	this.v0 = -(this.g * time_to_peak);
	this.elapsed_time = elapsed_time;
	this.current_time = elapsed_time;
	this.running = false;
	this.parabola_type = parabola_type;

	this.callback = function() { };

	this.initial_position;
};

Parabola.prototype.set_callback = function(callback) {
	this.callback = callback;
};

Parabola.prototype.private_solve_formula = function() {
	return (this.v0 * this.current_time) + (this.g * 0.5) * (this.current_time * this.current_time);
};

Parabola.prototype.private_get_initial_position = function(restarting = false) {

	if ( ! restarting && this.initial_position != "undefined")
		return this.initial_position;

	if (this.parabola_type == ParabolaType.OPEN_DOWN)
		return this.initial_position = this.rigid_body.get_y() + this.private_solve_formula();
	if (this.parabola_type == ParabolaType.OPEN_UP)
		return this.initial_position = this.rigid_body.get_y() - this.private_solve_formula();
	if (this.parabola_type == ParabolaType.OPEN_RIGHT)
		return this.initial_position = this.rigid_body.get_x() + this.private_solve_formula();
	if (this.parabola_type == ParabolaType.OPEN_LEFT)
		return this.initial_position = this.rigid_body.get_x() - this.private_solve_formula();
	
	return 0;
};

Parabola.prototype.initialize = function(rigid_body = 0) {
	if (rigid_body !== 0)
		this.rigid_body = rigid_body;
	this.current_time = this.elapsed_time;
	this.initial_position = this.private_get_initial_position(true);
	this.running = true;
	this.rigid_body.just_collide = false;
};

Parabola.prototype.update = function(delta) {
	if ( ! this.running)
		return;

	this.current_time += delta;
	if (this.parabola_type == ParabolaType.OPEN_DOWN)
		this.rigid_body.try_position(this.rigid_body.try_x, this.private_get_initial_position() - this.private_solve_formula());
	if (this.parabola_type == ParabolaType.OPEN_UP)
		this.rigid_body.try_position(this.rigid_body.try_x, this.private_get_initial_position() + this.private_solve_formula());
	if (this.parabola_type == ParabolaType.OPEN_RIGHT)
		this.rigid_body.try_position(this.private_get_initial_position() - this.private_solve_formula(), this.rigid_body.try_y);
	if (this.parabola_type == ParabolaType.OPEN_LEFT)
		this.rigid_body.try_position(this.private_get_initial_position() + this.private_solve_formula(), this.rigid_body.try_y);

	if (this.rigid_body.just_collide) {
		this.running = false;
		this.callback();
	}
};

var RigidBody = function(_x = 0, _y = 0, _width = 0, _height = 0) {
	this.type = 'RigidBody';
	this.alive = true;
	this.active = true;
	this.static = false;
	this.friendly_layer = 0;

	this.private_x = _x;
	this.private_y = _y;
	this.width = _width;
	this.height = _height;

	this.try_x = _x;
	this.try_y = _y;

	this.up_active_collide = true;
	this.down_active_collide = true;
	this.right_active_collide = true;
	this.left_active_collide = true;

	this.just_collide = false;
};

RigidBody.prototype.set_position_x = function(_x) {
	this.private_x = _x;
	this.try_x = _x;
};

RigidBody.prototype.set_position_y = function(_y) {
	this.private_y = _y;
	this.try_y = _y;
};

RigidBody.prototype.set_position = function(_x, _y) {
	this.private_x = _x;
	this.private_y = _y;
	this.try_x = _x;
	this.try_y = _y;
};

RigidBody.prototype.try_position = function(_x, _y) {
	this.try_x = _x;
	this.try_y = _y;
}

RigidBody.prototype.set_size = function(_width, _height) {
	this.width = _width;
	this.height = _height;
};

RigidBody.prototype.add_move = function(_x, _y) {
	this.try_x += _x;
	this.try_y += _y;
};

RigidBody.prototype.get_position = function() {
	return { x: this.private_x, y: this.private_y };
};

RigidBody.prototype.get_size = function() {
	return { width: this.width, height: this.height };
};

RigidBody.prototype.get_x = function() {
	return this.private_x;
};

RigidBody.prototype.get_y = function() {
	return this.private_y;
};

var Box = function(_x = 0, _y = 0, _width = 0, _height = 0) {
	this.type = 'Box';
	this.alive = true;
	this.active = true;
	this.static = true;
	this.friendly_layer = 0;

	this.private_x = _x;
	this.private_y = _y;
	this.width = _width;
	this.height = _height;

	this.up_active_collide = true;
	this.down_active_collide = true;
	this.right_active_collide = true;
	this.left_active_collide = true;
};

Box.prototype.get_position = function() {
	return { x: this.private_x, y: this.private_y };
};

Box.prototype.get_size = function() {
	return { width: this.width, height: this.height };
};

Box.prototype.get_x = function() {
	return this.private_x;
};

Box.prototype.get_y = function() {
	return this.private_y;
};

var Collider = function() {
	this.alter_A = new RigidBody();
};

Collider.prototype.collide = function(A, B) {
	if (A.private_x >= B.private_x + B.width || A.private_x + A.width <= B.private_x)
		return false;
	if (A.private_y >= B.private_y + B.height || A.private_y + A.height <= B.private_y)
		return false;

	if (B.type == 'Box')
		if ((A.private_x >= B.private_x && A.private_x + A.width <= B.private_x + B.width) &&
			(A.private_y >= B.private_y && A.private_y + A.height <= B.private_y + B.height))
			return false;

	return true;
};

Collider.prototype.collide_right = function(A, B) {
	if ( ! B.right_active_collide)
		return false;

	this.alter_A.private_x = A.private_x;
	this.alter_A.private_y = A.private_y;
	this.alter_A.width = A.try_x + A.width - A.private_x;
	this.alter_A.height = A.height;

	return this.collide(this.alter_A, B);
};
Collider.prototype.collide_left = function(A, B) {
	if ( ! B.left_active_collide)
		return false;

	this.alter_A.private_x = A.try_x;
	this.alter_A.private_y = A.private_y;
	this.alter_A.width = A.private_x + A.width - A.try_x;
	this.alter_A.height = A.height;

	return this.collide(this.alter_A, B);
};
Collider.prototype.collide_up = function(A, B) {
	if ( ! B.up_active_collide)
		return false;

	this.alter_A.private_x = A.private_x;
	this.alter_A.private_y = A.try_y;
	this.alter_A.width = A.width;
	this.alter_A.height = A.private_y + A.height - A.try_y;

	return this.collide(this.alter_A, B);
};
Collider.prototype.collide_down = function(A, B) {
	if ( ! B.down_active_collide)
		return false;

	this.alter_A.private_x = A.private_x;
	this.alter_A.private_y = A.private_y;
	this.alter_A.width = A.width;
	this.alter_A.height = A.try_y + A.height - A.private_y;

	return this.collide(this.alter_A, B);
};

var Physics = function() {
	this.bodies = [];

	this.private_ray_cast = new RigidBody();
	this.collider = new Collider();
};

Physics.prototype.add_rigid_body = function(body) {
	this.bodies.push(body);
};

Physics.prototype.private_move_right = function(body, other_body) {
	if (this.collider.collide_right(body, other_body)) {
		if (other_body.type == 'RigidBody')
			body.private_x = other_body.private_x - body.width;
		else if (other_body.type == 'Box')
			body.private_x = other_body.private_x + other_body.width - body.width;
		body.try_x = body.private_x;
		return true;
	}

	body.private_x = body.try_x;
	return false;
};
Physics.prototype.private_move_left = function(body, other_body) {
	if (this.collider.collide_left(body, other_body)) {
		if (other_body.type == 'RigidBody')
			body.private_x = other_body.private_x + other_body.width;
		else if (other_body.type == 'Box')
			body.private_x = other_body.private_x;
		body.try_x = body.private_x;
		return true;
	}

	body.private_x = body.try_x;
	return false;
};
Physics.prototype.private_move_up = function(body, other_body) {
	if (this.collider.collide_up(body, other_body)) {
		if (other_body.type == 'RigidBody')
			body.private_y = other_body.private_y + other_body.height;
		else if (other_body.type == 'Box')
			body.private_y = other_body.private_y;
		body.try_y = body.private_y;
		return true;
	}

	body.private_y = body.try_y;
	return false;
};
Physics.prototype.private_move_down = function(body, other_body) {
	if (this.collider.collide_down(body, other_body)) {
		if (other_body.type == 'RigidBody')
			body.private_y = other_body.private_y - body.height;
		else if (other_body.type == 'Box')
			body.private_y = other_body.private_y + other_body.height - body.height;
		body.try_y = body.private_y;
		return true;
	}

	body.private_y = body.try_y;
	return false;
};

Physics.prototype.update = function(delta) {
	for (var i = 0; i < this.bodies.length; i++) {
		var current_body = this.bodies[ i ];
		if ( ! current_body.alive) {
			this.bodies.splice(i, 1);
			i--;
			continue;
		}

		if ( ! current_body.active || current_body.static)
			continue;

		var direction_x = current_body.try_x - current_body.private_x;
		var direction_y = current_body.try_y - current_body.private_y;

		var was_colliding = false;
		for (var j = 0; j < this.bodies.length; j++) if (i != j) {
			var other_body = this.bodies[ j ];
			if ( ! other_body.active)
				continue;
			if (current_body.friendly_layer != 0 && current_body.friendly_layer == other_body.friendly_layer)
				continue;

			if (direction_x > 0)
				was_colliding = this.private_move_right(current_body, other_body);
			if (direction_x < 0)
				was_colliding = this.private_move_left(current_body, other_body);

			if (was_colliding)
				current_body.just_collide = true;

			if (direction_y < 0)
				was_colliding = this.private_move_up(current_body, other_body);
			if (direction_y > 0)
				was_colliding = this.private_move_down(current_body, other_body);

			if (was_colliding)
				current_body.just_collide = true;
		}
	}
};

Physics.prototype.ray_cast = function(position, size, friendly_layer = 0) {

	for (var i = 0; i < this.bodies.length; i++) {
		var current_body = this.bodies[ i ];
		if ( ! current_body.alive || ! current_body.active)
			continue;
		if (friendly_layer != 0 && friendly_layer == current_body.friendly_layer)
			continue;

		this.private_ray_cast.set_position(position.x, position.y);
		this.private_ray_cast.set_size(size.x, size.y);

		if (this.collider.collide(this.private_ray_cast, current_body))
			return true;
	}

	return false;
};
