
var Avatar = function(physics) {
	this.physics = physics;
	this.id = -1;
	this.rigid_body = new RigidBody();
	this.jump_parabola = new Parabola(this.rigid_body, 100, 0.5, ParabolaType.OPEN_DOWN, 0);
	this.fall_jump = new Parabola(this.rigid_body, 100, 0.5, ParabolaType.OPEN_DOWN, 0.5);
	this.status = { jumping: false, walking_left: 0, walking_right: 0, on_ground: false, is_sit_down: false };
	this.animator;
	this.input_listener;
};

Avatar.prototype.destructor = function() {
	this.rigid_body.alive = false;
	if (this.animator || this.animator.sprite)
		game_remove_child(this.animator.sprite);
};	

Avatar.prototype.jump = function() {
	if (this.jump_parabola.running)
		return;

	this.status.is_sit_down = false;
	this.jump_parabola.initialize();
};

Avatar.prototype.sit_down = function(val) {
	this.status.is_sit_down = val;
}

Avatar.prototype.walk_start_left = function(limit = -9999) {
	this.status.walking_left = limit;
};
Avatar.prototype.walk_start_right = function(limit = 9999) {
	this.status.walking_right = limit;
};
Avatar.prototype.walk_stop_left = function() {
	this.status.walking_left = 0;
};
Avatar.prototype.walk_stop_right = function() {
	this.status.walking_right = 0;
};

Avatar.prototype.start = function(_input_listener, x_start_position, y_start_position) {
	this.input_listener = _input_listener;
	this.input_listener.start(this);
	
	this.rigid_body.friendly_layer = 'player';

	var data = [
		{
			'name': 'idle',
			'package': 'images/all.json',
			'frames': ['idle/idle00.png', 'idle/idle01.png', 'idle/idle00.png', 'idle/idle01.png', 'idle/idle00.png', 'idle/idle00.png', 'idle/idle01.png', 'idle/idle02.png',
			'idle/idle03.png', 'idle/idle02.png', 'idle/idle04.png'],
			'delay': 1.0
		},
		{
			'name': 'walking',
			'package': 'images/all.json',
			'frames': ['walking/walking01.png', 'walking/walking02.png', 'walking/walking03.png',
			'walking/walking04.png'],
			'delay': 0.200
		},
		{
			'name': 'jump',
			'package': 'images/all.json',
			'frames': ['Jump/jump01.png'],
			'delay': 0.300
		},
		{
			'name': 'sit_down',
			'package': 'images/all.json',
			'frames': ['sit/sit.png'],
			'delay': 1.00
		}
	];
	this.animator = new Animator(data);
	
	this.rigid_body.set_position(x_start_position, y_start_position);
	this.rigid_body.set_size(48, 48);
	this.physics.add_rigid_body(this.rigid_body);
};

Avatar.prototype.private_use_ray_cast = function() {
	this.status.on_ground = this.physics.ray_cast(
		{'x': this.rigid_body.get_x(), 'y': this.rigid_body.get_y() + this.rigid_body.height - 1},
		{'x': this.rigid_body.width, 'y': 2},
		this.rigid_body.friendly_layer);
};

Avatar.prototype.private_handle_movement = function(delta) {
		
	if (this.status.on_ground && ! this.jump_parabola.running && ! this.fall_jump.running) {
		if (this.status.walking_left == this.status.walking_right ||
			(this.status.walking_left != 0 && this.status.walking_right != 0)) {

			if (this.status.is_sit_down)
				this.animator.select_animation('sit_down');
			else
				this.animator.select_animation('idle');
			
		} else if (this.status.walking_left) {
			this.animator.flip_x(true);
			if ( ! this.status.is_sit_down) {
				this.animator.select_animation('walking');
				var total_amount = delta * -200;
				total_amount = total_amount < this.status.walking_left ? this.status.walking_left : total_amount;
				this.rigid_body.add_move(total_amount, 0);
			}
		} else if (this.status.walking_right) {
			this.animator.flip_x(false);
			if ( ! this.status.is_sit_down) {
				this.animator.select_animation('walking');
				var total_amount = delta * 200;
				total_amount = total_amount > this.status.walking_right ? this.status.walking_right : total_amount;
				this.rigid_body.add_move(total_amount, 0);
			}
		}
	}

	if ( ! this.jump_parabola.running && ! this.fall_jump.running && ! this.status.on_ground) {
		this.animator.select_animation('jump');
		this.fall_jump.initialize();
	}

	if (this.jump_parabola.running) {
		this.status.is_sit_down = false;
		this.animator.select_animation('jump');
		this.jump_parabola.update(delta);
	}

	if (this.fall_jump.running) {
		this.status.is_sit_down = false;
		this.animator.select_animation('jump');
		this.fall_jump.update(delta);
	}
};

Avatar.prototype.update = function(delta) {
	this.private_use_ray_cast();
	this.private_handle_movement(delta);
	this.input_listener.update(this, delta);
	this.animator.put_position(this.rigid_body.get_x(), this.rigid_body.get_y());
	this.animator.update(delta);
};