var Platform = function(physics, _x, _y, _width, _height) {
	this.rigid_body = new RigidBody(_x, _y, _width, _height);
	this.rigid_body.static = true;

	this.rigid_body.up_active_collide = false;
	this.rigid_body.right_active_collide = false;
	this.rigid_body.left_active_collide = false;

	physics.add_rigid_body(this.rigid_body);


	var data = [
		{
			'name': 'idle',
			'package': 'images/atlas.json',
			'frames': ['red.png'],
			'delay': 1.0
		}
	];
	this.animator = new Animator(data);
	this.animator.put_position(this.rigid_body.get_x(), this.rigid_body.get_y());
	this.animator.put_size(this.rigid_body.get_size().width, this.rigid_body.get_size().height);
};

Platform.prototype.update = function(delta) {
	this.animator.update(delta);
};