
var Key = {
	X: 88,
	LEFT: 37,
	RIGHT: 39,
	UP: 38,
	DOWN: 40
};

var TypeEvent = {
	TYPED: 1,
	RELEASED: 2,
	MOUSE_TYPED: 3,
	MOUSE_RELEASED: 4,
	MOUSE_MOVE: 5
};

var SingletonKeyboardHandler = function() {
	this.private_key_listener = {};
};

SingletonKeyboardHandler.prototype.add_listener = function(key, type_event, callback) {
	if (typeof(this.private_key_listener[key]) == 'undefined') {
		
		this.private_key_listener[key] = {
			'TYPED' : [],
			'RELEASED': []
		};

		var obj_key = keyboard(key);
		var handle_key = this.private_key_listener[key];
		obj_key.press = () => {
			for (var i = 0; i < handle_key.TYPED.length; i++)
				handle_key.TYPED[ i ]();
		};
		obj_key.release = () => {
		  	for (var i = 0; i < handle_key.RELEASED.length; i++)
				handle_key.RELEASED[ i ]();
		};
	}

	if (type_event == TypeEvent.TYPED)
		this.private_key_listener[key].TYPED.push(callback);
	if (type_event == TypeEvent.RELEASED)
		this.private_key_listener[key].RELEASED.push(callback);
	
};

var SingletonMouseHandler = function() {
	this.private_key_listener = {};
};

SingletonMouseHandler.prototype.add_listener = function(type_event, callback) {
	if (typeof(this.private_key_listener[type_event]) == 'undefined') {
		this.private_key_listener[type_event] = [];

		var local_key_listener = this.private_key_listener;
		var mouse_handler = function(e) {
			for (var i = 0; i < local_key_listener[type_event].length; i++)
				local_key_listener[type_event][ i ](e.data.global.x, e.data.global.y);
		};

		if (type_event == TypeEvent.MOUSE_TYPED)
			app.stage.on('mousedown', mouse_handler);
		else if (type_event == TypeEvent.MOUSE_RELEASED)
			app.stage.on('mouseup', mouse_handler);
		else if (type_event == TypeEvent.MOUSE_MOVE)
			app.stage.on('mousemove', mouse_handler);
	}

	this.private_key_listener[type_event].push(callback);
};

window.enviro = window.enviro || {};
enviro.keyboard_handler = new SingletonKeyboardHandler();
enviro.mouse_handler = new SingletonMouseHandler();


