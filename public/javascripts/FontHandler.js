var FontHandler = function() {
	this.fonts = {};
	this.base_texture = {};
	this.letters = {};
	this.id_used_letters = {};
	this.current_font_name = '';
	this.area = {'x':0, 'y':0, 'width':0, 'height':0};
	this.current_text = '';
	this.current_initial_text_id = 0;
	this.current_text_id = 0;
	this.current_time_text_id = 0;
	this.waiting_next = true;
	this.current_time = 0;
	this.to_draw = [];
	this.letter_delay = 0;
	this.letter_x = 0;
	this.letter_y = 0;
};

FontHandler.prototype.load_font = function(font_name) {
	this.current_font_name = font_name;
	if (typeof(this.fonts[ font_name ]) !== 'undefined')
		return;

	this.fonts[ font_name ] = DEFAULT_FONT['font'];
	this.base_texture[ font_name ] = game_get_base_texture('images/bitmapfonts/default.png');

	this.letters[ font_name ] = {};
	this.id_used_letters[ font_name ] = {};
	for (let ascii in this.fonts[ font_name ]['chars']) {

		let letter = this.fonts[ font_name ]['chars'][ ascii ];
		let textrect = new Rectangle(letter['x'], letter['y'], letter['width'], letter['height']);

		this.letters[ font_name ][ ascii ] = [];
		this.id_used_letters[ font_name ][ ascii ] = 0;

		for (let i = 0; i < 30; i++) {
			let sprite = game_get_sprite(this.base_texture[ font_name ], textrect);
			sprite.visible = false;

			this.letters[ font_name ][ ascii ].push(sprite);
			game_add_child(sprite);
		}
	}
};

FontHandler.prototype.get_letter = function(ascii) {
	let letter = this.letters[ this.current_font_name ][ ascii ];
	let id = this.id_used_letters[ this.current_font_name ][ ascii ];

	if (id >= letter.length) {
		let letter = this.fonts[ this.current_font_name ]['chars'][ ascii ];
		let textrect = new Rectangle(letter['x'], letter['y'], letter['width'], letter['height']);
		let sprite = game_get_sprite(this.base_texture[ this.current_font_name ], textrect);
		this.letters[ this.current_font_name ][ ascii ].push(sprite);

		game_add_child(sprite);
	}
	
	this.id_used_letters[ this.current_font_name ][ ascii ] += 1;

	return letter[ id ];
};

FontHandler.prototype.free_letters = function() {
	this.current_time_text_id = this.current_initial_text_id;

	for (let ascii in this.id_used_letters[ this.current_font_name ])
		this.id_used_letters[ this.current_font_name ][ ascii ] = 0;

	for (let i in this.to_draw)
		this.to_draw[ i ]['letter'].visible = false;

	this.to_draw = [];
}

FontHandler.prototype.initialize = function(view_area, font_name = '') {
	if (font_name !== '')
		this.load_font(font_name);

	this.area = view_area;
	this.letter_x = view_area.x;
	this.letter_y = view_area.y;
};

FontHandler.prototype.set_text = function(text, letter_delay = -1) {
	this.current_text = text;
	this.current_time = 0;
	this.to_draw = [];
	if (letter_delay !== -1)
		this.letter_delay = letter_delay;

	this.next();
};

FontHandler.prototype.next = function() {
	if ( ! this.waiting_next)
		return;

	this.current_initial_text_id = this.current_text_id;
	this.waiting_next = false;
	this.free_letters();

	this.letter_x = this.area.x;
	this.letter_y = this.area.y;
	
	while (this.current_text_id < this.current_text.length) {
		
		let word = [];
		var id = this.current_text_id;

		let point_x = +this.letter_x;
		let point_y = +this.letter_y;

		while (id < this.current_text.length) {

			let ascii = this.current_text[ id ].charCodeAt(0);

			let letter = this.fonts[ this.current_font_name ]['chars'][ ascii ];
			let x = point_x + +letter['xoffset'];
			let y = point_y + +letter['yoffset'];

			word.push({'x': x, 'y': y, 'ascii': ascii, 'loaded': false, 'letter': this.get_letter(ascii)});

			id += 1;
			point_x += +letter['xadvance'];

			if (ascii == 32)
				break;
		}

		let exceed_limit_y = point_y + +this.fonts[ this.current_font_name ]['common']['lineHeight'] > this.area.y + this.area.height;
		if (exceed_limit_y)
			break;

		let exceed_limit_x = point_x > this.area.x + this.area.width;
		if (exceed_limit_x) {
			this.letter_x = this.area.x;
			this.letter_y += +this.fonts[ this.current_font_name ]['common']['lineHeight'];
			continue;
		}

		for (var i in word)
			this.to_draw.push(word[ i ]);

		this.letter_x = point_x;
		this.current_text_id = id;
	}
}

FontHandler.prototype.draw = function(letter) {
	if (letter['loaded'])
		return;

	letter['loaded'] = true;
	var sprite = letter['letter'];
	sprite.visible = true;
	sprite.position.x = letter['x'];
	sprite.position.y = letter['y'];
};

FontHandler.prototype.update = function(delta) {
	this.current_time += delta;
	if (this.current_time >= this.letter_delay && this.current_time_text_id < this.current_text_id) {

		this.current_time_text_id++;

		if (this.letter_delay <= 0)
			this.current_time_text_id = this.current_text_id;
		
		this.current_time = 0;
	}

	if (this.current_time_text_id >= this.current_text_id)
		this.waiting_next = true;

	let id_draw = 0;
	for (let i = this.current_initial_text_id; i < this.current_time_text_id; i++) {
		this.draw(this.to_draw[ id_draw ]);
		id_draw++;
	}
};

