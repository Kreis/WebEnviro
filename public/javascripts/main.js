var type = 'WebGL';
if ( ! PIXI.utils.isWebGLSupported()){
	type = 'canvas'
}

PIXI.utils.sayHello(type);
window.enviro = window.enviro || {};
window.enviro.stage_width = 1024;
window.enviro.stage_height = 600;

// alias
let
Application   = PIXI.Application,
Container     = PIXI.Container,
AutoDetect    = PIXI.autoDetectRenderer,
Sprite        = PIXI.Sprite,
Texture 	  = PIXI.Texture,
Loader        = PIXI.loader,
Resources     = PIXI.loader.resources,
Rectangle     = PIXI.Rectangle,
Text          = PIXI.Text,
BaseTexture   = PIXI.BaseTexture;

let app = new Application({
width: window.enviro.stage_width,
height: window.enviro.stage_height,
antialias: true,
transparent: false,
resolution: 1,
backgroundColor: 0x00CCCC
});

app.stage.interactive = true;
app.stage.hitArea = new Rectangle(0, 0, window.enviro.stage_width, window.enviro.stage_height);

function loadProgressHandler(loader, resource) {
	console.log('loading: ' + resource.url);
	console.log('progress: ' + loader.progress + '%'); 
}

var world;

function game_start() {
	world = new World();
  	world.start();
}

function game_loop(delta) {
  	world.update(delta);
}

function game_get_texture(package, texture_name) {
	if (package === 'bitmapfonts')
		return Resources[texture_name].texture;
	
	return Resources[package].textures[texture_name];
}

function game_get_base_texture(file_path) {
	return BaseTexture.fromImage(file_path);
}

function game_get_sprite(base_texture, rect) {
	return new Sprite(new Texture(base_texture, rect));
}

function game_add_child(sprite) {
	app.stage.addChild(sprite);
}

function game_remove_child(sprite) {
	app.stage.removeChild(sprite);
}

function setup() {
	console.log('setup');

	// var message = new Text('...', {fontFamily: 'Arial', fontSize: 32, fill: 'white'});

	game_start();
	app.ticker.add(delta => game_loop(delta));
}

Loader
.add('images/all.json')
.add('images/atlas.json')
.add('images/bitmapfonts/default.png')
.on('progress', loadProgressHandler)
.load(setup);

document.body.appendChild(app.view);

// sounds.load([
//   'sounds/crow.wav'
// ]);

// let sound_craw = sounds['sounds/crow.wav'];

