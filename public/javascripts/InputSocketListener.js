
enviro.InputSocketListener = function(state) {
	this.local_state = state;
	this.local_state.stop_x = state.x;
};

enviro.InputSocketListener.prototype.start = function(avatar) {
	var go = this.local_state;

	var event_state = function(state) {
		
		if ( ! state[ avatar.id ])
			return;

		if (state[ avatar.id ].s_jmp != go.s_jmp) {

			go.s_jmp = state[ avatar.id ].s_jmp;
			go.x = state[ avatar.id ].x;
			go.y = state[ avatar.id ].y;

			avatar.rigid_body.set_position_x(go.x);
			avatar.rigid_body.set_position_y(go.y);
			avatar.jump();
			return;
		}

		go.dir_x = state[ avatar.id ].dir_x;

		if (go.dir_x == 0 && ! state[ avatar.id ].sit)
			go.stop_x = state[ avatar.id ].x;
		
		go.flip_x = state[ avatar.id ].flip_x;
		go.sit = state[ avatar.id ].sit;
	};

	enviro.socket_handler.add_listener('on_state', event_state);
};

enviro.InputSocketListener.prototype.update = function(avatar, delta) {

	if (this.local_state.dir_x < 0) {
		avatar.walk_stop_right();
		avatar.walk_start_left();
	} else if (this.local_state.dir_x > 0) {
		avatar.walk_stop_left();
		avatar.walk_start_right();
	} else {
		var diff = this.local_state.stop_x - avatar.rigid_body.try_x;
		
		if (diff < 0) {
			avatar.walk_stop_right();
			avatar.walk_start_left(diff);
		} else if (diff > 0) {
			avatar.walk_stop_left();
			avatar.walk_start_right(diff);
		} else {
			avatar.walk_stop_left();
			avatar.walk_stop_right();
			avatar.animator.flip_x(this.local_state.flip_x);
			avatar.sit_down(this.local_state.sit);
		}
	}

};