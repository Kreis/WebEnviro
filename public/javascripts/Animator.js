
var Animator = function(data) {
	this.current_animation_name = '';
	this.current_animation_id = 0;
	this.current_time = 0;

	this.animations = {};

	this.sprite = 0;
	this.private_offset_x = 0;
	this.private_offset_y = 0;
	this.private_on_flip_x = false;
	this.private_on_flip_y = false;

	for (var i = 0; i < data.length; i++) {

		var current_animation = data[ i ];
		var current_name = current_animation.name;
		this.animations[ current_name ] = { delay: current_animation.delay, frames: []};

		for (var j = 0; j < current_animation.frames.length; j++) {
			this.animations[ current_name ].frames.push(game_get_texture(current_animation.package, current_animation.frames[ j ]));
		}
		
		if (this.sprite == 0) {
			this.current_animation_name = current_name;
			this.sprite = new Sprite(this.animations[ current_name ].frames[ 0 ]);
		}
	}

	game_add_child(this.sprite);
};

Animator.prototype.get_flip_x = function() {
	return this.private_on_flip_x;
};
Animator.prototype.get_flip_y = function() {
	return this.private_on_flip_y;
};

Animator.prototype.select_animation = function(name) {
	if (name == this.current_animation_name)
		return;
	
	this.current_animation_name = name;
	this.current_time = 0;
	this.current_animation_id = 0;
	this.sprite.texture = this.animations[ this.current_animation_name ].frames[ 0 ];
};

Animator.prototype.update = function(delta_time) {
	this.current_time += delta_time;
	if (this.current_time <= this.animations[ this.current_animation_name ].delay)
		return;

	this.current_animation_id++;
	if (this.current_animation_id >= this.animations[ this.current_animation_name ].frames.length)
		this.current_animation_id = 0;

	this.current_time = 0;
	this.sprite.texture = this.animations[ this.current_animation_name ].frames[ this.current_animation_id ];
};

Animator.prototype.put_position = function(_x, _y) {
	this.sprite.x = _x + this.private_offset_x;
	this.sprite.y = _y + this.private_offset_y;
};

Animator.prototype.put_size = function(_width, _height) {
	this.sprite.width = _width;
	this.sprite.height = _height;
}

Animator.prototype.flip_x = function(on) {
	this.private_on_flip_x = on;
	this.sprite.scale.x = on ? -1 : 1;
	this.private_offset_x = on ? this.sprite.width : 0;
}

Animator.prototype.flip_y = function(on) {
	this.private_on_flip_y = on;
	this.sprite.scale.y = on ? -1 : 1;
	this.private_offset_y = on ? this.sprite.height : 0;
}