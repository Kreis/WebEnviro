
enviro.InputListener = function() {
	this.snapshat_delay = 0.1;
	this.snapshat_current_time = 0;
};

enviro.InputListener.prototype.start = function(avatar) {

	var event_jump = function() {
		avatar.jump();
		var j = enviro.state_handler.get('s_jmp');
		var j2 = j == 0 ? 1 : 0;
		enviro.state_handler.put('s_jmp', j2);
		enviro.state_handler.put('x', avatar.rigid_body.get_x());
		enviro.state_handler.put('y', avatar.rigid_body.get_y());
		enviro.state_handler.put('sit', false);
	};
	enviro.keyboard_handler.add_listener(Key.X, TypeEvent.TYPED, event_jump);

	var event_end_jump = function () {
		enviro.state_handler.put('x', avatar.rigid_body.get_x());
		enviro.state_handler.put('y', avatar.rigid_body.get_y());	
	};

	avatar.jump_parabola.callback = event_end_jump;
	avatar.fall_jump.callback = event_end_jump;

	var update_direction = function() {
		var dir = avatar.status.walking_left ? -1 : (avatar.status.walking_right ? 1 : 0);
		enviro.state_handler.put('dir_x', dir);
		
		var flip_x = avatar.animator.get_flip_x();
		enviro.state_handler.put('flip_x', flip_x);

		enviro.state_handler.put('x', avatar.rigid_body.get_x());
		enviro.state_handler.put('y', avatar.rigid_body.get_y());
	}
	var event_walk_start_left = function() {
		avatar.walk_start_left();
		update_direction();
	};
	var event_walk_start_right = function() {
		avatar.walk_start_right();
		update_direction();
	};
	var event_walk_stop_left = function() {
		avatar.walk_stop_left();
		update_direction();
	};
	var event_walk_stop_right = function() {
		avatar.walk_stop_right();
		update_direction();
	};
	var event_sit_down = function() {
		avatar.sit_down( ! avatar.status.is_sit_down );
		enviro.state_handler.put('sit', avatar.status.is_sit_down);
	};
	enviro.keyboard_handler.add_listener(Key.LEFT, TypeEvent.TYPED, event_walk_start_left);
	enviro.keyboard_handler.add_listener(Key.RIGHT, TypeEvent.TYPED, event_walk_start_right);
	enviro.keyboard_handler.add_listener(Key.LEFT, TypeEvent.RELEASED, event_walk_stop_left);
	enviro.keyboard_handler.add_listener(Key.RIGHT, TypeEvent.RELEASED, event_walk_stop_right);
	enviro.keyboard_handler.add_listener(Key.DOWN, TypeEvent.TYPED, event_sit_down);
};

enviro.InputListener.prototype.update = function(avatar, delta) {

	if (this.snapshat_current_time > this.snapshat_delay) {
		this.snapshat_current_time = 0;
	}

	this.snapshat_current_time += delta;
};