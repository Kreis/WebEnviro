var socket = io();

window.enviro = window.enviro || {};
enviro.player_id = -1;
var SingletonSocketHandler = function() {
	this.events = {
		'on_state': []
	};
};

SingletonSocketHandler.prototype.add_listener = function(name_event, callback) {
	if ( ! this.events[ name_event ])
		return;

	this.events[ name_event ].push(callback);
};

// on
SingletonSocketHandler.prototype.on_state = function(data) {
	for (var i = 0; i < this.events.on_state.length; i++)
		this.events.on_state[ i ](data);
};

// emit
SingletonSocketHandler.prototype.emit_state = function() {
	var local_state = enviro.state_handler.get_local_state();
	local_state.id = enviro.player_id;
	socket.emit('state', local_state);
};

enviro.socket_handler = new SingletonSocketHandler();

socket.on('assign_id', function(data) {
	enviro.player_id = data;
});

socket.on('state', function(data) {
	enviro.socket_handler.on_state(data);
});