var _ = require('underscore');

var GameManager = (function () {

	// var tickSnapshot;
	var tickGame;
	var state = {};

	var start = function () {
		// tickSnapshot = setInterval(updateSnapshot, 100);
		tickGame = setInterval(update_game, 1000);
	};
	
	function update_game() {
		console.log('update');
		GameManager.emit_state(state);
	};

	function on_new_player(data) {
		state[ data.id ] = {
			id: data.id,
			x: 500,
			y: 500,
			s_jmp: 0,
			dir_x: 0,
			flip_x: false,
			sit: false
		};
	};

	function on_remove_client(data) {
		if ( ! state[ data.id ])
			return;

		delete state[ data.id ];
	}

	function on_state(data) {
		if ( ! state[ data.id ])
			return;

		state[ data.id ] = data;
	}

	return {
		emit_state: function(state) { },
		start: start,
		on_new_player: on_new_player,
		on_remove_client: on_remove_client,
		on_state: on_state
	};

})();
module.exports = GameManager;