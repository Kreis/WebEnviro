var io;

var SocketManager = (function () {
	var clients = {};

	var listen = function (_io) {
		io = _io;
		io.sockets.on('connection', function (socket) {
			var id_current = add_client(socket);
			socket.emit('assign_id', id_current);
			SocketManager.on_new_player({ id: id_current });

			socket.on('state', function(data) {
				SocketManager.on_state(data);
			});

			socket.on('disconnect', function () {
				SocketManager.on_remove_client({ id: id_current });
				SocketManager.remove_client(id_current);
			});
		});
	};
	
	function sendUpdate(snapshot) {
		io.emit('update', snapshot);
	}

	function guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	};

	function add_client(socket) {
		var uuid = guid();
		clients[ uuid ] = { 'socket': socket };

		return uuid;
	}

	function remove_client(id_current) {
		delete clients[ id_current ];
	}

	function emit_state(state) {
		io.emit('state', state);
	}

	return {
		listen: listen,
		sendUpdate: sendUpdate,
		on_remove_client: function(data) { },
		on_new_player: function(data) { },
		on_state: function(data) { },
		emit_state: emit_state,
		remove_client: remove_client
	};

})();
module.exports = SocketManager;